import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoadbarComponent } from './roadbar.component';

describe('RoadbarComponent', () => {
  let component: RoadbarComponent;
  let fixture: ComponentFixture<RoadbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoadbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoadbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
