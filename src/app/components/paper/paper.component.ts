import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-paper',
  templateUrl: './paper.component.html',
  styleUrls: ['./paper.component.scss']
})
export class PaperComponent implements OnInit {


  property_image  = {
    url: '/assets/imgs/content_image.jpg'
  }




  constructor() { }

  ngOnInit() {
  }

}
