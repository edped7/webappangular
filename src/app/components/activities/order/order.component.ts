import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {

  constructor() { }

  isError:boolean = false;
  isVerify:boolean = false;
  isRemake:boolean = true;

  //ToggleClass functionality
  verifyActivity(el){
    this.isError = !this.isError;
    this.isVerify = !this.isVerify;
    this.isRemake = !this.isRemake;

    // scroll to el with error
    el.scrollIntoView({behavior:"smooth"});
  }

  property_roadbar  = {
    back: true,
    back_road: "/lessons",
    label:"Ordenar",
    submenu: true,
  }

  ngOnInit() {
  }

  property_guide = {
    prev: "/lessons",
    next: "/quizz",
  }

  property_image  = {
    url: '/assets/imgs/web_aluno_fundo.jpg'
  }

  itemStringsLeft = [
    'Persistência de relações de trabalho compulsório. asdadsdsd sdasdasdasds ad dad d ada das dasdsdasdadasdasd asdasdasdasd das dad adasd dasdasdasdasdadasd adadasdadasd asda sd adsdsa dsada dasd. Persistência de relações de trabalhosdasdasdas compulsório. asdadsdsd sdasdasdasds ad dad d ada das dasdsdasdadasdasd asda das dad adasd dasdasdasd...',
    'Aqui deve conter um texto bem longo para representar as ações do usuário. Aqui deve conter um texto bem longo para representar as ações do usuário. Aqui deve conter um texto bem longo para representar as ações do usuário...',
    'Um texto menor, mas ainda assim participa da atividade',
    'Uma ideia simples de como o ordenar deve se comportar. Agora sim temos um ordernar mais funcionas.'
  ];

  property_guides  = {
    prev: "/activities/tf",
    next: "/resource",
  };

}
