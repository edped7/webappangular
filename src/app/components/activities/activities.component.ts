import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-activities',
  templateUrl: './activities.component.html',
  styleUrls: ['./activities.component.scss']
})
export class ActivitiesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  property_roadbar  = {
    back: true,
    back_road: "/lessons",
    label:"Atividades",
    submenu: true,
  }

}
