// ESSE TS FOI COPIADO E COLADO, NÃO HOUVE NENHUMA REFATORAÇÃO OU TRATAMENTO
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'; // comentei pois ainda não temos o módulo de rotas

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(public router: Router) { // comentado pois ainda não temos o módulo de rotas
  }

  ngOnInit() {
  }


  onLogin() {
    this.router.navigate(['/courses']); // comentado pois ainda não temos o módulo de rotas
  }

  property_logo  = {
    style: 'logotipe',
    logo_text: true,
    slogan:true,
  }



}
