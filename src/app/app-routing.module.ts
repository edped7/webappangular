import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';
import { CoursesComponent } from './components/courses/courses.component';
import { DisciplinesComponent } from './components/disciplines/disciplines.component';
import { LessonsComponent } from './components/lessons/lessons.component';
import { ResourceComponent} from './components/resource/resource.component';
import { ActivitiesComponent} from './components/activities/activities.component';
import { QuizzComponent} from './components/activities/quizz/quizz.component';
import { TfComponent} from './components/activities/tf/tf.component';
import { OrderComponent} from './components/activities/order/order.component';

const appRoutes: Routes = [
  {path: 'lessons', component: LessonsComponent},
  {path: 'navbar', component: NavbarComponent},
  {path: 'login', component: LoginComponent},
  {path: 'courses', component: CoursesComponent},
  {path: 'disciplines', component: DisciplinesComponent},
  {path: 'resource', component: ResourceComponent},
  {path: 'activities', component: ActivitiesComponent},
  {path: 'activities/quizz', component: QuizzComponent},
  {path: 'activities/tf', component: TfComponent},
  {path: 'activities/order', component: OrderComponent},

];


@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule],
  
})
export class AppRoutingModule {


}
