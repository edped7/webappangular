import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $(document).ready(function(){
      var wrap = $("#fixed_sidebar");

      wrap.on("scroll", function(e) {
          
        if (this.scrollTop > 147) {
          wrap.addClass("fix-search");
        } else {
          wrap.removeClass("fix-search");
        }
        
      });
    });
  }

}
