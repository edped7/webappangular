// ESSE TS FOI COPIADO E COLADO, NÃO HOUVE NENHUMA REFATORAÇÃO OU TRATAMENTO
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-resource',
  templateUrl: './resource.component.html',
  styleUrls: ['./resource.component.scss']
})
export class ResourceComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  property_roadbar  = {
    back: true,
    back_road: "/lessons",
    label:"Minha Aula",
    submenu: true,
  };

  property_guides  = {
    prev: "/disciplines",
    next: "/activities/quizz",
  };



}
