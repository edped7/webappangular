import { Component, OnInit, TemplateRef } from '@angular/core';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-disciplines',
  templateUrl: './disciplines.component.html',
  styleUrls: ['./disciplines.component.scss']
})
export class DisciplinesComponent implements OnInit {
  modalRef: BsModalRef;

  lessons: any[] = [
    {name: 'Nome da Disciplina'},
    {name: 'Nome da Disciplina'},
    {name: 'Nome da Disciplina'},
    {name: 'Nome da Disciplina'},
    {name: 'Nome da Disciplina'},
    {name: 'Nome da Disciplina'},
    {name: 'Nome da Disciplina'},
    {name: 'Nome da Disciplina'},

  ];

  property_roadbar  = {
    back: true,
    back_road: "/courses",
    label:"Minhas Disciplinas",
    submenu: false,
  }


  constructor(private modalService: BsModalService) { }

  ngOnInit() {
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
    event.stopPropagation();
  }

}
