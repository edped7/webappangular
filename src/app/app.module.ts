import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AlertModule, DatepickerModule } from 'ngx-bootstrap'; // apenas teste
import { FormsModule } from '@angular/forms'; // apenas teste

import { AccordionModule, BsDropdownModule, CollapseModule, ModalModule, SortableModule } from 'ngx-bootstrap';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';
import { LessonsComponent } from './components/lessons/lessons.component';
import { CoursesComponent } from './components/courses/courses.component';
import { ResourceComponent } from './components/resource/resource.component';
import { DisciplinesComponent } from './components/disciplines/disciplines.component';
import { AppRoutingModule } from './app-routing.module';
import { UserDetailsComponent } from './components/navbar/user-details/user-details.component';
import { RoadbarComponent } from './components/roadbar/roadbar.component';
import { ProgressbarComponent } from './components/progressbar/progressbar.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { PaperComponent } from './components/paper/paper.component';
import { ActivitiesComponent } from './components/activities/activities.component';
import { TitleComponent } from './components/paper/components/title/title.component';
import { VideoComponent } from './components/paper/components/video/video.component';
import { ImageComponent } from './components/paper/components/image/image.component';
import { TextComponent } from './components/paper/components/text/text.component';
import { ClueComponent } from './components/paper/components/clue/clue.component';
import { AudioComponent } from './components/paper/components/audio/audio.component';
import { LogoComponent } from './components/logo/logo.component';
import { QuizzComponent } from './components/activities/quizz/quizz.component';
import { TfComponent } from './components/activities/tf/tf.component';
import { OrderComponent } from './components/activities/order/order.component';
import { NumericComponent } from './components/activities/numeric/numeric.component';
import { FixedComponentComponent } from './components/fixed-component/fixed-component.component';
import { GuidesComponent } from './components/resource/guides/guides.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    LessonsComponent,
    CoursesComponent,
    ResourceComponent,
    DisciplinesComponent,
    UserDetailsComponent,
    RoadbarComponent,
    ProgressbarComponent,
    SidebarComponent,
    PaperComponent,
    ActivitiesComponent,
    TitleComponent,
    VideoComponent,
    ImageComponent,
    TextComponent,
    ClueComponent,
    AudioComponent,
    LogoComponent,
    QuizzComponent,
    TfComponent,
    OrderComponent,
    NumericComponent,
    FixedComponentComponent,
    GuidesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule, // apenas teste
    AlertModule.forRoot(), // apenas teste
    DatepickerModule.forRoot(), // apenas teste
    AccordionModule.forRoot(),
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    CollapseModule.forRoot(),
    SortableModule.forRoot(),
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
