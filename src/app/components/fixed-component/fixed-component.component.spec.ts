import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FixedComponentComponent } from './fixed-component.component';

describe('FixedComponentComponent', () => {
  let component: FixedComponentComponent;
  let fixture: ComponentFixture<FixedComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FixedComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FixedComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
