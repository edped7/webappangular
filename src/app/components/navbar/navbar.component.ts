// ESSE TS FOI COPIADO E COLADO, NÃO HOUVE NENHUMA REFATORAÇÃO OU TRATAMENTO
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  isIn = false; 
  toggleState() {
      let bool = this.isIn;
      this.isIn = bool === false ? true : false; 
  }

  property_logo  = {
    style: 'logotipe_internal',
    logo_text: true,
    slogan:false,
  }

}