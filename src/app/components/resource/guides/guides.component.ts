import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-guides',
  templateUrl: './guides.component.html',
  styleUrls: ['./guides.component.scss']
})
export class GuidesComponent implements OnInit {

  @Input() property;

  constructor() { }

  ngOnInit() {
  }

}
