// ESSE TS FOI COPIADO E COLADO, NÃO HOUVE NENHUMA REFATORAÇÃO OU TRATAMENTO
import { Component, OnInit, TemplateRef } from '@angular/core';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-lessons',
  templateUrl: './lessons.component.html',
  styleUrls: ['./lessons.component.scss']
})
export class LessonsComponent implements OnInit {

  oneAtATime = true;

  modalRef: BsModalRef;

  constructor(private modalService: BsModalService) { }

  ngOnInit() {
  }

  property_roadbar  = {
    back: true,
    back_road: "/disciplines",
    label:"Minhas Aulas",
    submenu: true,
  }


  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
    event.stopPropagation();
  }
}
