// ESSE TS FOI COPIADO E COLADO, NÃO HOUVE NENHUMA REFATORAÇÃO OU TRATAMENTO
import { Component, OnInit } from '@angular/core';
import { RoadbarComponent } from '../roadbar/roadbar.component';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss'],

})
export class CoursesComponent implements OnInit {
  cursos: any[] = [
    {name: 'A Idade Média é um período da história da Europa'},
    {name: 'Nome do Curso'},
    {name: 'Nome do Curso'},
    {name: 'Nome do Curso'},
    {name: 'Nome do Curso'},
    {name: 'Nome do Curso'},

  ];

  property_roadbar  = {
    back: false,
    back_road: "/login",
    label:"Meus Cursos",
    submenu: false,
  }

  constructor() { }

  ngOnInit() {
  }

}
