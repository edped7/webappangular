import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-quizz',
  templateUrl: './quizz.component.html',
  styleUrls: ['./quizz.component.scss']
})
export class QuizzComponent implements OnInit {

  property_image  = {
    url: '/assets/imgs/web_aluno_fundo.jpg'
  }

  constructor() { }

  isError:boolean = false;
  isVerify:boolean = false;
  isRemake:boolean = true;

  //ToggleClass functionality
  verifyActivity(el){
    this.isError = !this.isError;
    this.isVerify = !this.isVerify;
    this.isRemake = !this.isRemake;

    // scroll to el with error
    el.scrollIntoView({behavior:"smooth"});
  }

  ngOnInit() {
  }

  property_roadbar  = {
    back: true,
    back_road: "/lessons",
    label:"Quizz",
    submenu: true,
  }

  property_guides  = {
    prev: "/resource",
    next: "/activities/tf",
  };

  // Activities

  allActivity = [
    {
      text: 'Persistência de relações de trabalho compulsório. asdadsdsd sdasdasdasds ad dad d ada das dasdsdasdadasdasd asdasdasdasd das dad adasd dasdasdasdasdadasd adadasdadasd asda sd adsdsa dsada dasd. Persistência de relações de trabalhosdasdasdas compulsório. asdadsdsd sdasdasdasds ad dad d ada das dasdsdasdadasdasd asda das dad adasd dasdasdasd', 
      feedback: 'Aqui vai o Feedback pois provavelmente você errou a alternativa.',
      template: 1,
    },
  ];
  
}