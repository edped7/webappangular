import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fixed-component',
  templateUrl: './fixed-component.component.html',
  styleUrls: ['./fixed-component.component.scss']
})
export class FixedComponentComponent implements OnInit {

  property_image  = {
    url: '/assets/imgs/web_aluno_fundo.jpg'
  }


  constructor() {

   
   }
   toogle_fixed_comp:boolean = false;

   toogle_fixed_component(){
     this.toogle_fixed_comp = !this.toogle_fixed_comp;
   }

  ngOnInit() {
  }

}
