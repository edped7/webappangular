import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tf',
  templateUrl: './tf.component.html',
  styleUrls: ['./tf.component.scss']
})
export class TfComponent implements OnInit {

  property_image  = {
    url: '/assets/imgs/web_aluno_fundo.jpg'
  }


  constructor() { }

  isError:boolean = false;
  isVerify:boolean = false;
  isRemake:boolean = true;

  //ToggleClass functionality
  verifyActivity(el){
    this.isError = !this.isError;
    this.isVerify = !this.isVerify;
    this.isRemake = !this.isRemake;

    // scroll to el with error
    el.scrollIntoView({behavior:"smooth"});
  }

  ngOnInit() {
  }

  property_roadbar  = {
    back: true,
    back_road: "/lessons",
    label:"Verdadeiro ou Falso",
    submenu: true,
  }

  property_guides  = {
    prev: "/activities/quizz",
    next: "/activities/order",
  };

}
